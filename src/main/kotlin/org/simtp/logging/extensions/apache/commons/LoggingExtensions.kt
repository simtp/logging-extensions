@file:Suppress("unused")

package org.simtp.logging.extensions.apache.commons

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import kotlin.reflect.KClass

val log : () -> Log = { CommonsLog().log }

open class CommonsLog(name : String) {

    constructor() : this("CommonsLog")
    constructor(klass : KClass<*>) : this(klass.java.name)

    val log = LogFactory.getLog(name)!!
}

inline infix fun Log?.trace(message : () -> String ) {
    if (this != null && this.isTraceEnabled) {
        this.trace(message())
    }
}

inline infix fun Log?.debug(message : () -> String ) {
    if (this != null && this.isDebugEnabled) {
        this.debug(message())
    }
}

/**
 */
inline infix fun Log?.info(message : () -> String ) {
    if (this != null && this.isInfoEnabled) {
        this.info(message())
    }
}

/**
 */
inline infix fun Log?.warn(message : () -> String ) {
    if (this != null && this.isWarnEnabled) {
        this.warn(message())
    }
}

/**
 */
inline infix fun Log?.error(message : () -> String ) {
    if (this != null && this.isErrorEnabled) {
        this.error(message())
    }
}

inline infix fun Log?.tracex(provider : () -> Pair<String, Throwable> ) {
    if (this != null && this.isTraceEnabled) {
        val pair = provider()
        this.trace(pair.first, pair.second)
    }
}


inline infix fun Log?.debugx(provider : () -> Pair<String, Throwable> ) {
    if (this != null && this.isDebugEnabled) {
        val pair = provider()
        this.debug(pair.first, pair.second)
    }
}

fun Log?.debug(provider: () -> String, ex : Exception ) {
}
/**
 */
inline infix fun Log?.infox(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isInfoEnabled) {
        val pair = provider()
        this.info(pair.first, pair.second)
    }
}

/**
 */
inline infix fun Log?.warnx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isWarnEnabled) {
        val pair = provider()
        this.warn(pair.first, pair.second)
    }
}

/**
 */
inline infix fun Log?.errorx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isErrorEnabled) {
        val pair = provider()
        this.error(pair.first, pair.second)
    }
}

