package org.simtp.logging.extensions.slf4j

//import org.slf4j.Logger
//import org.slf4j.LoggerFactory
//
//val log : () -> Logger = { SLF4JLogger().logger}
//
//
//open class SLF4JLogger(name : String) {
//
//    constructor() : this("SLF4JLogger")
//    constructor(klass : Class<*>) : this(klass.name)
//
//    val logger = LoggerFactory.getLogger(name)!!
//}
//
///**
// */
//inline infix fun Logger?.trace(message : () -> String ) {
//    if (this == null) {
//        return
//    }
//    if (this.isTraceEnabled) this.trace(message())
//}
//
//inline infix fun Logger?.debug(message : () -> String ) {
//    if (this == null) {
//        return
//    }
//    if (this.isDebugEnabled) this.debug(message())
//}
//
///**
// */
//inline infix fun Logger?.info(message : () -> String ) {
//    if (this == null) {
//        return
//    }
//    if (this.isInfoEnabled) this.info(message())
//}
//
///**
// */
//inline infix fun Logger?.warn(message : () -> String ) {
//    if (this == null) {
//        return
//    }
//    this.warn(message())
//}
//
///**
// */
//inline infix fun Logger?.error(message : () -> String ) {
//    if (this == null) {
//        return
//    }
//    this.error(message())
//}
//
//inline infix fun Logger?.traceEx(provider : () -> Pair<String, Throwable> ) {
//    if (this == null) {
//        return
//    }
//    if (this.isTraceEnabled) {
//        val pair = provider()
//        this.trace(pair.first, pair.second)
//    }
//}
//
//
//inline infix fun Logger?.debugEx(provider : () -> Pair<String, Throwable> ) {
//    if (this == null) {
//        return
//    }
//    if (this.isDebugEnabled) {
//        val pair = provider()
//        this.debug(pair.first, pair.second)
//    }
//}
///**
// */
//inline infix fun Logger?.infoEx(provider : () -> Pair<String, Throwable> ) {
//    if (this == null) {
//        return
//    }
//    if (this.isInfoEnabled) {
//        val pair = provider()
//        this.info(pair.first, pair.second)
//    }
//}
//
///**
// */
//inline infix fun Logger?.warnEx(provider : () -> Pair<String, Throwable> ) {
//    if (this == null) {
//        return
//    }
//    val pair = provider()
//    this.warn(pair.first, pair.second)
//}
//
///**
// */
//inline infix fun Logger?.errorEx(provider : () -> Pair<String, Throwable> ) {
//    if (this == null) {
//        return
//    }
//    val pair = provider()
//    this.error(pair.first, pair.second)
//}
//
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

open class SLF4JLogger(name : String) {

    constructor() : this("SLF4JLogger")
    constructor(klass : KClass<*>) : this(klass.java.name)

    val logger = LoggerFactory.getLogger(name)
}

/**
 */
inline infix fun Logger?.trace(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isTraceEnabled) this.trace(message())
}

inline infix fun Logger?.debug(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isDebugEnabled) this.debug(message())
}

/**
 */
inline infix fun Logger?.info(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isInfoEnabled) this.info(message())
}

/**
 */
inline infix fun Logger?.warn(message : () -> String ) {
    if (this == null) {
        return
    }
    this.warn(message())
}

/**
 */
inline infix fun Logger?.error(message : () -> String ) {
    if (this == null) {
        return
    }
    this.error(message())
}

inline infix fun Logger?.traceEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isTraceEnabled) {
        val pair = provider()
        this.trace(pair.first, pair.second)
    }
}


inline infix fun Logger?.debugEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isDebugEnabled) {
        val pair = provider()
        this.debug(pair.first, pair.second)
    }
}
/**
 */
inline infix fun Logger?.infoEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isInfoEnabled) {
        val pair = provider()
        this.info(pair.first, pair.second)
    }
}

/**
 */
inline infix fun Logger?.warnEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    val pair = provider()
    this.warn(pair.first, pair.second)
}

/**
 */
inline infix fun Logger?.errorEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    val pair = provider()
    this.error(pair.first, pair.second)
}

