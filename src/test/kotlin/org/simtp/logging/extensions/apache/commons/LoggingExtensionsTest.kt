package org.simtp.logging.extensions.apache.commons

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.junit.Test
import org.mockito.Mockito.*
import org.simtp.logging.extensions.with

/**
 * Created by Grant Little grant@grantlittle.me
 */
class LoggingExtensionsTest {

    @Test
    fun testTrace_Null() {
        val log : Log? = null
        log trace { "TEST" }
        //There should be no NPE
    }

    @Test
    fun testTrace_eNotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isTraceEnabled).thenReturn(false)
        log trace { "TEST" }
        verify(log).isTraceEnabled
        verify(log, never()).trace("TEST")
    }

    @Test
    fun testTrace_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isTraceEnabled).thenReturn(true)
        log trace { "TEST" }
        verify(log).isTraceEnabled
        verify(log).trace("TEST")
    }

    @Test
    fun testTracex_Null() {
        val log : Log? = null
        val ex = Exception()
        log tracex { "TEST" with ex}
        //There should be no NPE
    }

    @Test
    fun testTracex_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isTraceEnabled).thenReturn(false)
        val ex = Exception()
        log tracex { "TEST" with ex }
        verify(log).isTraceEnabled
        verify(log, never()).trace("TEST", ex)
    }

    @Test
    fun testTracex_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isTraceEnabled).thenReturn(true)
        val ex = Exception()
        log tracex { "TEST" with ex }
        verify(log).isTraceEnabled
        verify(log).trace("TEST", ex)
    }


    // Debug
    @Test
    fun testDebug_Null() {
        val log : Log? = null
        log debug { "TEST" }
        //There should be no NPE
    }

    @Test
    fun testDebug_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isDebugEnabled).thenReturn(false)
        log debug { "TEST" }
        verify(log).isDebugEnabled
        verify(log, never()).debug("TEST")
    }

    @Test
    fun testDebug_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isDebugEnabled).thenReturn(true)
        log debug { "TEST" }
        verify(log).isDebugEnabled
        verify(log).debug("TEST")
    }

    @Test
    fun testDebugx_Null() {
        val log : Log? = null
        val ex = Exception()
        log debugx { "TEST" with ex}
        //There should be no NPE
    }

    @Test
    fun testDebugx_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isDebugEnabled).thenReturn(false)
        val ex = Exception()
        log debugx { "TEST" with ex }
        verify(log).isDebugEnabled
        verify(log, never()).debug("TEST", ex)
    }

    @Test
    fun testDebugx_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isDebugEnabled).thenReturn(true)
        val ex = Exception()
        log debugx { "TEST" with ex }
        verify(log).isDebugEnabled
        verify(log).debug("TEST", ex)
    }

    // Info
    @Test
    fun testInfo_Null() {
        val log : Log? = null
        log info { "TEST" }
        //There should be no NPE
    }

    @Test
    fun testInfo_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isInfoEnabled).thenReturn(false)
        log info { "TEST" }
        verify(log).isInfoEnabled
        verify(log, never()).info("TEST")
    }

    @Test
    fun testInfo_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isInfoEnabled).thenReturn(true)
        log info { "TEST" }
        verify(log).isInfoEnabled
        verify(log).info("TEST")
    }

    @Test
    fun testInfox_Null() {
        val log : Log? = null
        val ex = Exception()
        log infox { "TEST" with ex}
        //There should be no NPE
    }

    @Test
    fun testInfox_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isInfoEnabled).thenReturn(false)
        val ex = Exception()
        log infox { "TEST" with ex }
        verify(log).isInfoEnabled
        verify(log, never()).info("TEST", ex)
    }

    @Test
    fun testInfox_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isInfoEnabled).thenReturn(true)
        val ex = Exception()
        log infox { "TEST" with ex }
        verify(log).isInfoEnabled
        verify(log).info("TEST", ex)
    }

    // Warn
    @Test
    fun testWarn_Null() {
        val log : Log? = null
        log warn { "TEST" }
        //There should be no NPE
    }

    @Test
    fun testWarn_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isWarnEnabled).thenReturn(false)
        log warn { "TEST" }
        verify(log).isWarnEnabled
        verify(log, never()).warn("TEST")
    }

    @Test
    fun testWarn_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isWarnEnabled).thenReturn(true)
        log warn { "TEST" }
        verify(log).isWarnEnabled
        verify(log).warn("TEST")
    }

    @Test
    fun testWarnx_Null() {
        val log : Log? = null
        val ex = Exception()
        log warnx { "TEST" with ex}
        //There should be no NPE
    }

    @Test
    fun testWarnx_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isWarnEnabled).thenReturn(false)
        val ex = Exception()
        log warnx { "TEST" with ex }
        verify(log).isWarnEnabled
        verify(log, never()).warn("TEST", ex)
    }

    @Test
    fun testWarnx_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isWarnEnabled).thenReturn(true)
        val ex = Exception()
        log warnx { "TEST" with ex }
        verify(log).isWarnEnabled
        verify(log).warn("TEST", ex)
    }

    // Error
    @Test
    fun testError_Null() {
        val log : Log? = null
        log error { "TEST" }
        //There should be no NPE
    }

    @Test
    fun testError_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isErrorEnabled).thenReturn(false)
        log error { "TEST" }
        verify(log).isErrorEnabled
        verify(log, never()).error("TEST")
    }

    @Test
    fun testError_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isErrorEnabled).thenReturn(true)
        log error { "TEST" }
        verify(log).isErrorEnabled
        verify(log).error("TEST")
    }

    @Test
    fun testErrorx_Null() {
        val log : Log? = null
        val ex = Exception()
        log errorx { "TEST" with ex}
        //There should be no NPE
    }

    @Test
    fun testErrorx_NotEnabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isErrorEnabled).thenReturn(false)
        val ex = Exception()
        log errorx { "TEST" with ex }
        verify(log).isErrorEnabled
        verify(log, never()).error("TEST", ex)
    }

    @Test
    fun testErrorx_Enabled() {
        val log : Log = mock(Log::class.java)
        `when`(log.isErrorEnabled).thenReturn(true)
        val ex = Exception()
        log errorx { "TEST" with ex }
        verify(log).isErrorEnabled
        verify(log).error("TEST", ex)
    }

    @Test
    fun testNonInlineVersion() {
        val log : Log = LogFactory.getLog(this.javaClass)
        log info  {"Test"}
    }


}