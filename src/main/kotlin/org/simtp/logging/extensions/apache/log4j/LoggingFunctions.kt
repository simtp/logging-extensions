@file:Suppress("unused")
package org.simtp.logging.extensions.apache.log4j

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.apache.log4j.Logger
import kotlin.reflect.KClass

val log : () -> Log = { Log4JLog().log }

open class Log4JLog(name : String) {

    constructor() : this("CommonsLog")
    constructor(klass : KClass<*>) : this(klass.java.name)

    val log = LogFactory.getLog(name)!!
}

/**
 */
inline infix fun Logger?.trace(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isTraceEnabled) this.trace(message())
}

inline infix fun Logger?.debug(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isDebugEnabled) this.debug(message())
}

/**
 */
inline infix fun Logger?.info(message : () -> String ) {
    if (this == null) {
        return
    }
    if (this.isInfoEnabled) this.info(message())
}

/**
 */
inline infix fun Logger?.warn(message : () -> String ) {
    if (this == null) {
        return
    }
    this.warn(message())
}

/**
 */
inline infix fun Logger?.error(message : () -> String ) {
    if (this == null) {
        return
    }
    this.error(message())
}

inline infix fun Logger?.traceEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isTraceEnabled) {
        val pair = provider()
        this.trace(pair.first, pair.second)
    }
}


inline infix fun Logger?.debugEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isDebugEnabled) {
        val pair = provider()
        this.debug(pair.first, pair.second)
    }
}
/**
 */
inline infix fun Logger?.infoEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    if (this.isInfoEnabled) {
        val pair = provider()
        this.info(pair.first, pair.second)
    }
}

/**
 */
inline infix fun Logger?.warnEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    val pair = provider()
    this.warn(pair.first, pair.second)
}

/**
 */
inline infix fun Logger?.errorEx(provider : () -> Pair<String, Throwable> ) {
    if (this == null) {
        return
    }
    val pair = provider()
    this.error(pair.first, pair.second)
}

