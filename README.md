This project provides Kotlin logging extensions for various logging libraries
to help simplify common logging messages within a kotlin project.

The idea is not to replace existing logging frameworks, but simply to provide extension functions that 
supplement investment in existing logging frameworks but making them easier to use and and less verbose.

## Usage
```kotlin
import org.simtp.logging.extensions.apache.commons.*

fun myMethod(param : String, log : Log? = null) {
    try {
        // .. do something
        log debug { "We are doing something here with $param" }
    } catch (e : Exception) {
        log errorx { "An error was encountered" with e }
    }
}
```
The extensions make sure calls to the underlying logger are null safe. So if a
value of log is not provided to the method then nothing will happen. But it is, 
then before the log write is performed it will effectively do the following:-

```kotlin
if (null == log) return
if (log.isDebugEnabled() {
    log.debug("We are doing something here with $param")
}
```

There are currently extensions for

- Apache Commons Logging
- Log4J
- SLF4J 

## Creating a Log/Logger Object

To simplify the creation of logger objects you can use the following companion object pattern

```kotlin
import org.simtp.logging.extensions.apache.commons.*
class MyClass {
    companion object : CommonsLog(MyClass::class)
    
    fun myMethod(param : String) {
        try {
            // .. do something        
        } catch (ex : Exception) {
            log errorx { "Input param $param" with ex }
        }
    }
}
```

## Usage Not Covered By Extensions?

As the purpose of this library is to provide useful extensions, you can always use the existing Log/Logger
of your chosen framework to perform more advanced use cases if needed.

```kotlin
fun myMethod(param : String, log : Log? = null) {
    try {
        // .. do something        
    } catch (ex : Exception) {
        if (log.isErrorE)
        log.error("Input param $param", ex)
        log errorx { "Input param $param" with ex }
    }
}

```